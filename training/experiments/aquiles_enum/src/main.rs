mod exercise;
use exercise::{Health, WebEvent, GameGenres};

fn main() {

    // Bools
    let run_enum: bool = false;

    // Enum
    if run_enum == true {
        // Enum game
        exercise::enum_game(Health::Healthy(80));
        exercise::enum_game(Health::Injured(50));
        exercise::enum_game(Health::Dead);

        // Enum WebEvent
        exercise::game_inspect(WebEvent::PageLoad);
        exercise::game_inspect(WebEvent::PageUnload);
        exercise::game_inspect(WebEvent::KeyPress('c'));
        exercise::game_inspect(WebEvent::Paste("Copy and Paste".to_string()));
        exercise::game_inspect(WebEvent::Click{x: 40, y: 55});

        // Enum Alias
        type Generos = GameGenres;
        exercise::games_genres(Generos::Role);
        exercise::games_genres(Generos::Adventure);
        exercise::games_genres(Generos::Metroidvania);
        exercise::games_genres(Generos::Racing);
    }

}
