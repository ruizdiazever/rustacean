pub enum Health {
    Healthy(u32),
    Injured(u32),
    Dead,
}


pub enum WebEvent {
    PageLoad,
    PageUnload,
    KeyPress(char),
    Paste(String),
    Click { x: i64, y: i64 }
}


pub enum GameGenres {
    Role,
    Adventure,
    Metroidvania,
    Racing
}


pub fn enum_game(input: Health) {
    match input {
        Health::Healthy(value) => println!("El jugador está saludable con vida {}", value),
        Health::Injured(value) => println!("El jugador está herido con {} puntos de daño", value),
        Health::Dead => println!("El jugador está muerto con 0 de vida"),
    }
}


pub fn game_inspect(event: WebEvent) {
    match event {
        WebEvent::PageLoad => println!("Page loaded"),
        WebEvent::PageUnload => println!("Page unloaded"),
        WebEvent::KeyPress(c) => println!("Pressed '{}'.", c),
        WebEvent::Paste(s) => println!("Pasted \"{}\".", s),
        WebEvent::Click { x, y } => {
            println!("clicked at x={}, y={}.", x, y);
        },
    }
}


pub fn games_genres(genre: GameGenres) {
    match genre {
        GameGenres::Role => println!("Juegos de rol"),
        GameGenres::Adventure => println!("Juegos de aventura"),
        GameGenres::Metroidvania => println!("Juegos de tipo Metroidvania"),
        GameGenres::Racing => println!("Juegos de carrera")
    }
}
