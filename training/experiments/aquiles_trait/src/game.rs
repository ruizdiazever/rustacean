pub struct Game {
    pub title: &'static str,
    pub release: u32,
    pub genre: &'static str,
    pub cool: bool
}

pub trait GameFeatures {
    fn new(title: &'static str, release: u32,
        genre: &'static str, cool: bool) -> Self;
    fn is_cool(&self) ->  &'static str;
    fn print_this(&self) -> String;
}

impl GameFeatures for Game {
    fn new(title: &'static str, release: u32,
        genre: &'static str, cool: bool) -> Game {
        Game {title, release, genre, cool}
    }

    fn is_cool(&self) ->  &'static str {
        if self.cool == true {
            "is cool"
        } else {
            "not cool"
        }
    }

    fn print_this(&self) -> String {
        let output = format!("{} released in {} of genre {} is {}!", self.title, self.title, self.genre, self.cool);
        output
    }
}
