mod example;
mod exercise;
mod game;
use example::{Sheep, Animal};
use exercise::{Rectangle, CalcRectangle};
use game::{Game, GameFeatures};


fn main() {
    // Example
    let mut dolly: Sheep = Animal::new("Dolly");
    dolly.talk();
    dolly.shear();
    dolly.talk();
    dolly.name();

    // Rectangle
    let rectangle = Rectangle {
        length: 30 as f32,
        width: 50 as f32,
    };

    println!("\nRectangle, length {}, width {}", rectangle.length, rectangle.width);
    println!("Area: {}", rectangle.area());
    println!("Diff of sides: {}", rectangle.diff_absolute());
    println!("Perimeter: {}", rectangle.perimeter());
    println!("Diagonal: {}", rectangle.diagonal());

    // Game
    let game_1: Game = Game {
        title: "Portal",
        release: 2007,
        genre: "Puzzle",
        cool: true
    };

    let game_2: Game = Game {
        title: "Portal 2",
        release: 2011,
        genre: "Puzzle",
        cool: true
    };

    println!("\n{}", game_1.print_this());
    println!("{}", game_2.print_this());
}
