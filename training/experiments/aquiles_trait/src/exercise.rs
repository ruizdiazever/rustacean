pub struct Rectangle {
    pub length: f32,
    pub width: f32,
}

pub trait CalcRectangle {
    fn area(&self) -> f32;
    fn diff_absolute(&self) -> f32;
    fn perimeter(&self) -> f32;
    fn diagonal(&self) -> f32;
}

impl CalcRectangle for Rectangle {
    fn area(&self) -> f32 {
        self.length * self.width
    }

    fn diff_absolute(&self) -> f32 {
        (self.length - (self.width)).abs()
    }

    fn perimeter(&self) -> f32 {
        2.0 * (self.length + self.width)
    }

    fn diagonal(&self) -> f32 {
        let diagonal = (self.length.powi(2) + self.width.powi(2)).sqrt();
        diagonal
    }
}
