fn main() {
    let test = "Sofia".to_string();
    let some = string_reverse(test);
    println!("{:?}", some);
}


// "子猫", "猫子"
fn string_reverse(input: String) -> String {
    let input_vec: Vec<&str> = input.split("").collect();
    let mut input_rev_vec = Vec::new();
    let len_input: usize = input.len() + 1;

    for character in (0..len_input).rev() {
        input_rev_vec.push(input_vec[character]);
    }
    input_rev_vec.join("")
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tested_suite(){
        // Normal
        assert_eq!(string_reverse("Sofia".to_string()), "aifoS".to_string());

        // Empty string
        assert_eq!(string_reverse("".to_string()), "".to_string());

        // A word
        assert_eq!(string_reverse("robot".to_string()), "tobor".to_string());

        // A capitalized word
        assert_eq!(string_reverse("Ramen".to_string()), "nemaR".to_string());

        // A sentence with punctuation
        assert_eq!(string_reverse("I'm hungry!".to_string()), "!yrgnuh m'I".to_string());

        // A palindrome
        assert_eq!(string_reverse("racecar".to_string()), "racecar".to_string());

        // My favorite palindrome
        assert_eq!(string_reverse("Hannah".to_string()), "hannaH".to_string());

        // An even-sized word
        assert_eq!(string_reverse("drawer".to_string()), "reward".to_string());

        // Grapheme clusters
        assert_eq!(string_reverse("uüu".to_string()), "uüu".to_string());
    }

    #[test]
    fn to_fix(){
        // Wide characters
        assert_eq!(string_reverse("子猫".to_string()), "猫子".to_string());
    }

}
