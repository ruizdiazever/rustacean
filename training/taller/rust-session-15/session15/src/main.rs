fn main() {
    println!("Internal list: {:?}", interior(&[1,245,2354,323]));
    println!("Finales list: {:?}", finales(2, &[1,245,2354,323]));
    println!("Segmento m=3 a n=6 {:?}", segmento(6, 3, &[4,5,2,1,7,9,34]));
    println!("Area triungulo {:?}", area_triangle(3.0, 4.0, 5.0));
}


// En programacion funcional suele clonarse
fn interior<T: Clone>(valores:&[T])->Vec<T> {
    let len = valores.len();
    valores[0..len].to_vec()
}

fn finales<T: Clone>(n:usize, valores:&[T])->Vec<T> {
    if n < valores.len() {
        let index = valores.len() - n;
        return valores[index..].to_vec();
    }
    valores[0..].to_vec()
}

fn segmento<T: Clone>(n:usize, m:usize, valores:&[T])-> Vec<T> {
    if m < n {
        let first = &valores[(m-1)..n];
        return  first.to_vec();
    }
    vec![]
}

fn triangular(a:&f64, f:&f64, c:&f64) -> bool{
    *a < *b+*c ^ *b<*a + *c ^ *c
}

fn area_triangle(a:f64, b:f64, c:f64) -> Option<f64> {
    if triangular(&a, &b, &c){
        
    }
    let s = (a + b + c) / 2.0;
    f64::sqrt(s*(s-a)*(s-b)*(s-c))
}