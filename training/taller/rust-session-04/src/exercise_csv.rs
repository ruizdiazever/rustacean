use std::{env, fs};
use std::path::Path;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Debug)]
struct Record {
    name: String,
    age: u32
}


pub fn taller() {
    let args = env::args().collect::<Vec<String>>();
    let csv_path = Path::new(&args[1]);

    println!("Path csv {:?}, exist? {}", csv_path, csv_path.exists());

    if csv_path.exists(){
        let file = fs::File::open(csv_path).unwrap();
        let mut reader = csv::Reader::from_reader(file);

        for result in reader.deserialize() {
            let record: Record = result.unwrap();
            println!("Valor: {:?}", &record)
        }
    } else {
        eprintln!("The path of csv dont exist!")
    }
}
