use std::{env, fs};
use std::path::Path;
use std::io::{BufReader, BufRead, Write};
use regex::Regex;


pub fn read_file() {
    // Variables
    let args: Vec<String> = env::args().collect();
    let name_file = &args[1];
    let files = Path::new("./src/files");
    let mut valid_files: Vec<String> = Vec::new();

    // Destine of files
    let path_output = Path::new("./src/files/poema_edited.txt");
    let mut destine = fs::File::create(path_output).unwrap();
    let path_output_re = Path::new("./src/files/poema_edited_re.txt");
    let mut destine_re = fs::File::create(path_output_re).unwrap();

    // Collect path of valid files
    for entry in files.read_dir().expect("read_dir call failed") {
        if let Ok(entry) = entry {
            let file = entry.path();
            match file.extension(){
                Some(ext) => {
                    if ext == "txt" {
                        let archivo = file.to_str().unwrap().to_string();
                        valid_files.push(archivo.clone());
                    }
                },
                None => {}
            };
        }
    }

    // Iteration files
    for file in valid_files.iter() {
        let path = Path::new(file);
        let filename = path.file_stem().unwrap().to_str();

        if filename == Some(name_file) {
            println!("Path: {:?}", path);
            println!("Filename: {}", filename.unwrap());

            // Read
            let file = fs::File::open(path).unwrap();
            let reader = BufReader::new(file);

            for (i, line) in reader.lines().enumerate() {
                let padded = format!("{:02}", i);
                let verso = line.expect("Error to read the line");

                // With Regex
                let re = Regex::new(r"imagina\w*").unwrap();
                let modified_verso = re.replace_all(&verso, "");
                // Write
                let verso_to_write = modified_verso.to_string() + "\n";
                let bytes_writed = destine_re.write(&verso_to_write.as_bytes());
                println!("{}, Writed: {:?}", padded, &bytes_writed);

                // Without Regex
                let words: Vec<&str> = verso
                    .split_whitespace()
                    .filter(|word| !word.starts_with("imagina"))
                    .collect();
                let filtered_line = words.join(" ");
                // Write
                let verso_to_write_2 = filtered_line.to_string() + "\n";
                let bytes_writed_2 = destine.write(&verso_to_write_2.as_bytes());
                println!("{}, Writed: {:?}", padded, &bytes_writed_2);

            }
        }
    }
}
