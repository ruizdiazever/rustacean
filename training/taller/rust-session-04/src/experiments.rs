use std::io;

pub fn read_input() -> io::Result<()> {
    let mut input = String::new();

    println!("Please write something: ");
    io::stdin().read_line(&mut input)?;

    println!("You typed: {}", input.trim());

    Ok(())
}


pub fn get_string() -> io::Result<String> {
    let mut buffer = String::new();

    println!("Please write something again: ");
    io::stdin().read_line(&mut buffer)?;

    Ok(buffer)
}
