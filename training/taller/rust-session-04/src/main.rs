use std::io;
mod experiments;
mod exercise_write;
mod exercise_csv;


fn main() -> io::Result<()> {
    // Bools
    let run_experiments: bool = false;
    let run_write: bool = false;
    let run_csv: bool = true;

    // Personal experiments
    if run_experiments == true {
        experiments::read_input()?;
        let text = experiments::get_string()?;
        println!("Hello, your text is: {}", text);
    }

    // Write module
    if run_write == true {
        exercise_write::read_file();
    }

    // CSV module
    if run_csv == true {
        exercise_csv::taller()
    }

    Ok(())
}
