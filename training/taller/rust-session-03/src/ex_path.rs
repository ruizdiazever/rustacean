use std::path::{Path, PathBuf};


pub fn path_exercise() {
    // Check if exist path
    let files = Path::new("./src/files");
    println!("Exist the files? {:?}", files.try_exists());

    // Is a dir?
    println!("Is a directory? {:?}", files.is_dir());

    // Vectors
    let mut py_files: Vec<PathBuf> = Vec::new();
    let mut py_files_string: Vec<String> = Vec::new();
    let mut path_list: Vec<String> = Vec::new();

    // Read files
    for entry in files.read_dir().expect("read_dir call failed") {
        if let Ok(entry) = entry {
            // String
            match entry.path().to_str() {
                Some(path) => {
                    path_list.push(path.to_string())
                },
                None => println!("Lala")
            }

            // PathBuf
            let file = entry.path();
            match file.extension() {
                Some(ext) => {
                    if ext == "py" {
                        py_files.push(file)
                    }
                },
                None => println!("Error to get extension")
            }

            // String
            let file_string = entry.path();
            match file_string.extension() {
                Some(ext) => {
                    if ext == "py"{
                        py_files_string.push(file_string.to_string_lossy().to_string())
                    }
                },
                None => println!("Error to get extension")
            }
        }
    }

    println!("Py files: {:?}", py_files);
    println!("Path full list (PathBuf): {:?}", path_list);
    println!("Path full list (String): {:?}", py_files_string)
}
