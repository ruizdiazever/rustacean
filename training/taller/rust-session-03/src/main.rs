mod ex_path;
mod ex_result;
mod ex_env;
mod ex_read_fs;

fn main() {
    println!("\nExercise 1: Result");
    ex_result::result_exercise();
    println!("\nExercise 2: std::Path");
    ex_path::path_exercise();
    println!("\nExercise 3: std::env");
    ex_env::exercise_env();
    println!("\nExercise 4: read file");
    ex_read_fs::read_file();
}
