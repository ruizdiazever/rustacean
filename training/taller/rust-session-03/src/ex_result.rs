pub fn result_exercise() {
    // Basic
    println!("Hello, world!");
    let x: Result<i32, &str> = Ok(10);
    println!("Is ok? {}", x.is_ok());

    // Result
    let value_1: i64 = 100;
    let result_1 = positive(value_1);
    println!("{:?}", result_1);

    // Err
    let value_2: i64 = -200;
    let result_2 = positive(value_2);
    println!("{:?}", result_2);

    // Unrwap
    let value_3: i64 = 500;
    let result_3 = positive(value_3).unwrap();
    println!("{:?}", result_3);

    // Match
    let value_4: i64 = -200;
    match positive(value_4) {
        Ok(value) => println!("Value ok {}", value),
        Err(value) => eprintln!("Error {}", value)
    }
}


fn positive(value: i64) -> Result<i64, &'static str> {
    if value >= 0 {
        return Ok(value)
    } else {
        Err("No positive")
    }
}
