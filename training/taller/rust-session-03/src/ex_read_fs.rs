use std::{env, fs};
use std::path::Path;
use std::io::prelude::*;
use std::io::BufReader;


pub fn read_file() {
    // https://doc.rust-lang.org/std/fs/struct.File.html

    // Variables
    let args: Vec<String> = env::args().collect();
    let arg_to_read = &args[1];
    let path = Path::new("./src/files");

    let mut file_ext: String = "".to_string();
    let mut file_name: String = "".to_string();
    let mut file_path: String = "".to_string();

    for entry in path.read_dir().expect("read_dir call failed") {

        if let Ok(entry) = entry {
            // Path file
            match entry.path().to_str() {
                Some(path) => {
                    file_path = String::from(path.to_string())
                },
                None => println!("Error to read file path!")
            }

            // Extension
            match entry.path().extension() {
                Some(ext) => {
                    let file_extension = ext.to_str().unwrap();
                    file_ext = String::from(file_extension);
                },
                None => println!("Error to read extension of file.")
            }

            // File name
            match entry.path().file_stem() {
                Some(name) => {
                    let name_string = name.to_string_lossy().to_string();
                    file_name = String::from(name_string);
                },
                None => println!("Some error reading files.")
            };

            // Result
            if (file_ext == "txt") & (&file_name == arg_to_read) {
                let mut file_1 = fs::File::open(&file_path).unwrap();
                let file_2 = fs::File::open(&file_path).unwrap();

                //Option 1, limited
                let content_mode_1 = fs::read_to_string(entry.path()).unwrap();
                println!("Mode 1: \n{:?}", content_mode_1);

                // Option 2, with File::open
                let mut contents_1 = String::new();
                file_1.read_to_string(&mut contents_1).unwrap();
                println!("\nMode 2: \n{}", contents_1);

                // Option 3
                let mut buf_reader = BufReader::new(file_2);
                let mut contents_2 = String::new();
                buf_reader.read_to_string(&mut contents_2).unwrap();
                println!("Mode 3: \n{}", contents_2);

            }
        }
    }
}
