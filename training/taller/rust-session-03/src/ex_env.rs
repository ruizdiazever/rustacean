use std::env;

pub fn exercise_env() {
    // Args
    for (i, argument) in env::args().enumerate() {
        println!("{i} --> {argument}");
    }

    // Collect args
    let args: Vec<String> = env::args().collect();
    println!("Args collected: {:?}", args);

    // Current path
    let current_path = env::current_dir();
    match current_path {
        Ok(path) => {
            println!("Current path is: {:?}", path)
        },
        Err(_) => println!("Some error reading the current path")
    }
}
