use std::io;
use rand::Rng;
use std::cmp::Ordering;


fn main() {
    game_remix();
    game_original();
}

fn game_original() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng()
        .gen_range(0..100);

    loop {
        println!("Please input a number: ");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("\nLess!"),
            Ordering::Greater => println!("\nGreater!"),
            Ordering::Equal => {
                println!("\nWin");
                break;
            }
        }
    }
}

fn game_remix() {
    println!("Remix game: please, remember to do me!\n");
}
