mod pirate;
mod remake;


fn main() {
    println!("\n####### Pirate game 1: ");

    let boat_a = pirate::Boat1 {
        position: -25.3, 
        velocity: 5.5, 
        name: String::from("Bellisima"),
        };
    
    let boat_b = pirate::Boat2 {
        position: 19.75, 
        velocity: 6.7, 
        name: String::from("Carbonara"),
    };
    
    pirate::pirate_remix(boat_a, boat_b);

    println!("\n####### Pirate game 2: ");
    let boats = [
        remake::Boat{name: "Bellisima".to_string(), position: -25.3, velocity: 5.5},
        remake::Boat{name: "Carbonara".to_string(), position: 19.75, velocity: 6.7},
        remake::Boat{name: "Lampi".to_string(), position: 29.75, velocity: 9.7},
    ];

    remake::pirate_remake(&boats)
}
