pub struct Boat1 {
    pub position: f32,
    pub velocity: f32,
    pub name: String
}


pub struct Boat2 {
    pub position: f32,
    pub velocity: f32,
    pub name: String
}


pub fn pirate_remix(boat1: Boat1, boat2: Boat2) {
    // Island
    let position_island: f32 = -3.3;

    // Distance from island
    let boat1_distance = (boat1.position - (position_island)).abs();
    let boat2_distance = (boat2.position - (position_island)).abs();

    // time = distance / vel
    let boat_1_time = boat1_distance / boat1.velocity;
    let boat_2_time = boat2_distance / boat2.velocity;

    // Output
    let msg_1 = format!("Boat 1 '{}' \n\t* Distance: {} km \n\t* Speed: {} km/h \n\t* Time to arrive: {} hs", 
                        &boat1.name, &boat1_distance, &boat1.velocity, &boat_1_time);
    let msg_2 = format!("Boat 2 '{}' \n\t* Distance: {} km \n\t* Speed: {} km/h \n\t* Time to arrive: {} hs", 
                        &boat2.name, &boat2_distance, &boat2.velocity, &boat_2_time);
    println!("\n{}\n{}", msg_1, msg_2);

    // Control
    if boat_1_time < boat_2_time {
        println!("The boat {} is more fast, arrive in {} hs to the island!", &boat1.name, &boat_1_time)
    } else {
        println!("The boat {} is more fast, arrive in {} hs to the island!", &boat2.name, &boat_2_time)
    }
}
