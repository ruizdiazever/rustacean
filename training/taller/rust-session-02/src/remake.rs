use std::collections::HashMap;


pub struct Boat {
    pub name: String,
    pub position: f32,
    pub velocity: f32,
}


pub fn pirate_remake(boats: &[Boat]) {
    let mut results = HashMap::new();
    let island: f32 = -3.3;

    for boat in boats {
        let name = &boat.name;
        let time = calc_time(&boat.position, &island, &boat.velocity);
        results.insert(time.to_string(), name.to_string());
    }

    let key_with_max_value = results.iter().max_by_key(|entry | entry.1).unwrap();
    let value_in_float: f32 = key_with_max_value.0.parse().unwrap();
    
    println!("\nThe winner boat is {} with {:?} hs", &key_with_max_value.1, value_in_float);
    println!("Results: {:?}", &results);
}

fn calc_time(position: &f32, destination: &f32, velocity: &f32) -> f32 {
    let distance = (position - (destination)).abs();
    let time = distance / velocity;

    time
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc_time(){
        let time_expected: f32 = 3.4072165;
        let result = calc_time(&29.75, &-3.3, &9.7);

        assert_eq!(time_expected, result);
    }
}
