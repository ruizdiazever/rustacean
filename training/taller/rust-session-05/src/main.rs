mod ex_some;

fn main() {
    println!("Hello, world!");
    let name = "Ever".to_string();
    println!("{}", game(name).unwrap());

    color_hex(Semaphore::Green(150));
    color_hex(Semaphore::Yellow(100));
    color_hex(Semaphore::Red(50));


    // The return value of the function is an option
    let result = ex_some::divide(2.0, 3.0);

    // Pattern match to retrieve the value
    match result {
        // The division was valid
        Some(x) => println!("Result: {x}"),
        // The division was invalid
        None    => println!("Cannot divide by 0"),
    }
}


fn game(name: String) -> Option<String>{
    if name.is_empty() {
        None
    } else {
        let output = format!("Hi {:?}", name);
        Some(output)
    }
}


enum Semaphore {
    Green(u8),
    Yellow(u8),
    Red(u8)
}


fn color_hex(color: Semaphore) {
    match color {
        Semaphore::Green(value) => println!("{:?} is verde", value),
        Semaphore::Yellow(value) => println!("{:?} is marillo", value),
        Semaphore::Red(value) => println!("{:?} is rojo", value)
    }
}
