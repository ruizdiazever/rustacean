# Microservices with Axum

## CURL

```bash
curl -iX GET "http://127.0.0.1:8080"
# GET ORDER
curl -iX GET "http://127.0.0.1:8080/orders/e0da5f9e-ef02-40e7-a45e-558874255dba"
# ADD ITEM TO ORDER
curl -iX POST "http://127.0.0.1:8080/orders/e0da5f9e-ef02-40e7-a45e-558874255dba/items"
# DELETE ITEM FROM ORDER
curl -iX DELETE "http://127.0.0.1:8080/orders/e0da5f9e-ef02-40e7-a45e-558874255dba/items/3"
```
