use uuid::Uuid;
use serde::{Deserialize};


#[derive(Deserialize)]
pub struct AddItem {
    pub product_id: Uuid,
    pub quantity: i32,
}
