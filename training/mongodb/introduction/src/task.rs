use chrono::{NaiveDate, Utc};

// Unit struct, to work with generics
#[derive(Debug)]
pub struct Taskbasic;
#[derive(Debug)]
pub struct Tasktuple(pub String, pub bool);
#[derive(Debug, Clone)]
pub struct Task {
    pub name: String,
    #[allow(dead_code)]
    created: NaiveDate,
    pub due_date: Option<NaiveDate>,
    pub done: bool
}

// Constructor, block of implementation
impl Task {
    pub fn new(name: String, due_date: Option<NaiveDate>, done: bool) -> Task {
        Task {
            name,
            created: Utc::now().date_naive(), 
            due_date,
            done,
        }
    }
}