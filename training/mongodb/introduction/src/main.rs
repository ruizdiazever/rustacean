mod in_memory_task_repo;
mod task;
mod task_repo;
use dotenv;

use mongodb::{options::ClientOptions, Client};
use task::{Task, Taskbasic, Tasktuple};
use std::{fmt::Debug, time::Duration, env, error::Error};
use tokio::{time::sleep, join, spawn, select};

use crate::in_memory_task_repo::InMemoryTaskRepo;
use crate::task_repo::TaskRepo;


#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv::dotenv().ok();
    let example_basic: bool = false;
    let example_sync: bool = false;
    let example_async: bool = false;

    // EXAMPLE BASIC
    if example_basic {
        let repo = InMemoryTaskRepo::new();
        basic_example();
        use_task_repo(repo);
    }

    // EXAMPLE SYNC
    if example_sync {
        let value_1: String = do_long_lasting_op(2500);
        println!("\nValue: {value_1}");
        let value_2: String = do_long_lasting_op(1500);
        println!("\nValue: {value_2}");
    }

    // EXAMPLE ASYNC
    if example_async {
        let value_future_1 = do_long_lasting_op_with_tokio(2500);
        let value_future_2 = do_long_lasting_op_with_tokio(1500);

        let (value_f_1, value_f_2) = join!(value_future_1, value_future_2);
        println!("\nValue: {value_f_1}");
        println!("\nValue: {value_f_2}");
    }

    // EXAMPLE MONGODB
    let msg = "You must set MONGODB_URI enviroment var!".to_string();
    let client_uri = env::var("MONGODB_URI").expect(&msg);
    let options = ClientOptions::parse(client_uri).await?;
    let client = Client::with_options(options)?;
    let jh_db = spawn( async move { client.list_database_names(None, None).await });
    let jh_progress = spawn(async  {
        loop {
            sleep(Duration::from_millis(20)).await;
            print!(".")
        }
    });
    print!("Databases");
    select! {
        _ = jh_progress => {

        }
        db_names = jh_db => {
            let db_names = db_names??;
            println!("");
            for name in db_names {
                println!("- {name}");
            }
        }
    }

    Ok(())
}


fn basic_example() {
    let task = Taskbasic {};
    let task_tuple = Tasktuple("Ever".to_string(), true);
    println!("\ntask: {task:?}, tasktuple: {task_tuple:?}\n");
}

fn use_task_repo(mut repo: impl TaskRepo + Debug) {
    // You can declare this like this = <T: TaskRepo + Debug>(mut repo: T)

    // Tasks
    let task_traditional: Task = Task::new("To homework".to_string(), None, true);
    let task_work: Task = Task::new("To work".to_string(), None, true);

    // New repo and adding tasks
    repo.add(task_traditional);
    repo.add(task_work);

    // Prints
    println!("repo: {repo:?}\n");
    println!("Tasks in repo: {:?}\n", repo.list());
    println!("Repo info: {repo}");
}

fn do_long_lasting_op(ms: u64) -> String {
    std::thread::sleep(Duration::from_millis(ms));
    format!("Hola from the future ({ms}) ms")
}

async fn do_long_lasting_op_with_tokio(ms: u64) -> String {
    sleep(Duration::from_millis(ms)).await;
    format!("Hola from the future ({ms}) ms")
}
