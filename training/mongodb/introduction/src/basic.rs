fn basic() {
    let mut whom: &str = "world";
    println!("Hello, {whom}!");
    whom = "planet";
    println!("Bye, {whom}!");

    let x: String = String::from("This is not a &str");

    // Borrow checker: verificador de prestamos de memoria
    // Reference: con '&' tomo prestado el valor 
    use_string(&x);

    // De esta manera puede seguir usandose aca
    println!("x = {x}");

    // Shadowing
    let times: &str = "5";
    let times: Result<i32, _> = times.parse();
    println!("times = {times:?}");

    // Method 1, Ok and Err
    match times {
        Ok(v) => {
            println!("times = {v}");
        }
        Err(_) => {
            println!("ERR: couldn't convert");
        }
    }

    // Method 2 with if
    if let Ok(v) = times {
        println!("times = {v}");
    }

    // Option
    let times_2: &str = "5";
    let times_2:Option<i32> = times_2.parse().ok();
    match times_2 {
        Some(v) => {
            println!("times = {v}");
        }
        None => {
            println!("ERR: couldn't convert");
        }
    }

    // Option alter
    if let Some(v) = times_2 {
        println!("times = {v}");
    }
}   

fn use_string(s: &str) {
    println!("Inside of user_string: s = {s}");
}