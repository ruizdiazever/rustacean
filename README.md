# Rust

<img src="./files/lang.webp" alt="drawing" width="100"/>

My learning path with Rust, my new favorite language, sorry Python :(

## Steps 😁

1. [Official Book](https://doc.rust-lang.org/book/)
2. [Rust by Example](https://doc.rust-lang.org/rust-by-example/index.html)
3. [Rustling](https://github.com/rust-lang/rustlings)
4. First project with [Axum](https://github.com/tokio-rs/axum), [SQLx](https://diesel.rs/), [Tokio](https://tokio.rs/), [GraphQL](https://graphql.org/), [PostgreSQL 15](https://www.postgresql.org/) and [Flutter](https://flutter.dev/)

## Useful commands 🐧

```bash
cargo new $name --bin         # New project
cargo new communicator --lib  # Create a library
cargo build --release         # Compilation with optimizations
cargo check                   # Checker
cargo run                     # Compilate and exec
cargo doc --open              # Create doc with your dependencies in local
rustup update                 # Update version of Rust
cargo new --csv none $name    # Create a project without Git
cargo test
```

## Resources 📝

- [Crates](https://crates.io/)
- [Debugging in VS Code](https://www.youtube.com/watch?v=XPfSTRs02oY&list=PLojDVPvSO1DjYj8bMcMOU3KzLbRww-3Eb&index=6)
- [What Rust is it?](https://www.whatrustisit.com/)
- [First steps with Docker + Rust](https://dev.to/rogertorres/first-steps-with-docker-rust-30oi)
- [Tutorial in Spanish](https://blog.adrianistan.eu/rust-101-tutorial-rust-espanol)

## Favorite project ❤️

- [Pingora](https://blog.cloudflare.com/how-we-built-pingora-the-proxy-that-connects-cloudflare-to-the-internet/): like NGINX but writed in Rust, consumes about 70% less CPU and 67% less memory
- [Iced](https://github.com/iced-rs/iced): cross-platform GUI library focused on simplicity and type-safety
- [Dioxus](https://dioxuslabs.com/): user interfaces that run anywhere
- [Arti](https://gitlab.torproject.org/tpo/core/arti/): new version of Tor in Rust
- [Diesel](https://diesel.rs/): a Safe, Extensible ORM and Query Builder for Rust
- [Tokio](https://tokio.rs/): an asynchronous runtime for the Rust programming language
- [Yew](https://yew.rs/): modern Rust framework for creating multi-threaded front-end web apps using WebAssembly
- [Pop!_OS](https://pop.system76.com/): a Linux distro with a future desktop environment developed in Rust
- [Quickwit](https://quickwit.io/): something like ElasticSearch
- [Fluvio](https://www.fluvio.io/docs/): something like Kafka
- [GreptimeDB](https://github.com/GreptimeTeam/greptimedb): hybrid time-series/analytics processing database in the cloud with [GreptimeDB Dashboard](https://github.com/GreptimeTeam/dashboard)
- [Warp](https://www.warp.dev/): a modern, Rust-based terminal with AI.
- [Zed](https://zed.dev/): is a high-performance, multiplayer code editor from the creators of Atom and Tree-sitter.
- [SurrealDB](https://surrealdb.com/): multi-model database for tomorrow's applications.
- [Pola](https://www.pola.rs/): alternative of Pandas coded in Rust.
- [Rnote](https://github.com/flxzt/rnote): sketch and take handwritten notes in Rust.
